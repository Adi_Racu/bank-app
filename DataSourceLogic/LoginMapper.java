package DataSourceLogic;
import DomainLogic.Login;
import java.sql.*;
public class LoginMapper {
	public static boolean verifyLoginId(int login_id)
	{
		
		try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sql = "SELECT * FROM login WHERE login_id = ?";
            PreparedStatement prepSt = con.prepareStatement(sql);

            prepSt.setInt(1, login_id);

            ResultSet rs = prepSt.executeQuery();
            while(rs.next()){
        	
            return true;
            }
            rs.close();
           return false;
            
        } catch (Exception e) {
            System.out.println("Eroare in Domain, LoginMapper, verifyLoginId(int login_id)" + e);
            return false;
        }
	}
	
	public static Login getLoginByUsername(String username)
	{
		Login l = new Login(username, "-");
        try {


        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sql = "SELECT login_id, password, role FROM login WHERE username = ?";
            PreparedStatement prepSt = con.prepareStatement(sql);

            prepSt.setString(1, username);

            ResultSet rs = prepSt.executeQuery();
            while(rs.next()){
        	
            l.setLogin_id(rs.getInt("login_id"));
            l.setPassword(rs.getString("password"));
            l.setRole(rs.getInt("role"));
            }
            rs.close();
            
        } catch (Exception e) {
            System.out.println("Eroare in Domain, LoginMapper, getLoginByUsername(username)" + e);
        }
        return l;
	}
	
    public static boolean getLoginAdmin(String username, String password) {

        try {
            DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sql = "SELECT username, password, role FROM login WHERE (username = ? and password = ?)";
            PreparedStatement prepSt = con.prepareStatement(sql);

            prepSt.setString(1, username);
            prepSt.setString(2, password);

            ResultSet rs = prepSt.executeQuery();

            int r = 0;
            if (rs.next()) {
                r = rs.getInt("role");
            }
            rs.close();
            if (r == 1) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println("Eroare in DataSource, LoginMapper, getLoginAdmin(String username, String password)" + e);
            return false;
        }
    }
      
  public static boolean getLoginClient(String username, String password){
        try{
            DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;
        
            String sql = "SELECT username, password, role FROM login WHERE (username = ? and password = ?)";
            PreparedStatement prepSt = con.prepareStatement(sql);
        
            prepSt.setString(1, username);
            prepSt.setString(2, password);
        
            ResultSet rs = prepSt.executeQuery();
        
            int r = 0;
            if(rs.next()){
                r = rs.getInt("role");
            }
            rs.close();
            if(r == 0){
                return true;
            }else{
                return false;
             }
            
        }catch(Exception e){
            System.out.println("Eroare in DataSource, LoginMapper, getLoginClient(String username, String password)" + e);
            return false;
        }           
    }
            
     public static int getLoginId(Login l){
        int id_login = 0;
        try{
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;
            String sql = "SELECT login_id FROM login WHERE username = ? AND password = ?";
            PreparedStatement prepSt = con.prepareCall(sql);
            prepSt.setString(1, l.getUsername());
            prepSt.setString(2, l.getPassword());
            ResultSet rs = prepSt.executeQuery();
            while(rs.next()){
                id_login = rs.getInt("id");
            }
            rs.close();
        }catch(SQLException e){
            System.out.println("Eroare in DataSource, LoginMapper, getLoginId(Login l)" + e);
        }
        return id_login;
    }

    public static int getLoginId(String username, String password) {

        int r = -1;
        try {

        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sql = "SELECT login_id FROM login WHERE (username = ? and password = ?)";
            PreparedStatement prepSt = con.prepareStatement(sql);

            prepSt.setString(1, username);
            prepSt.setString(2, password);

            ResultSet rs = prepSt.executeQuery();

            if (rs.next()) {
                r = rs.getInt("login_id");
            }

        } catch (Exception e) {
            System.out.println("Eroare in DataSource, LoginMapper, getLoginId(username, password)" + e);
        }
        return r;
    }
    
      
    
    public static Login getId(String username, String password){
        Login log = new Login();
        try{
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;
            String sql = "SELECT * FROM login WHERE username = ? AND password = ?";
            PreparedStatement prepSt = con.prepareStatement(sql);
            prepSt.setString(1, username);
            prepSt.setString(2, password);
            ResultSet rs = prepSt.executeQuery();
            while(rs.next()){
                log.setLogin_id(rs.getInt("id_login"));
                log.setUsername(rs.getString("username"));
                log.setPassword(rs.getString("password"));
                log.setRole(rs.getInt("role"));
            }
            rs.close();
        }catch(SQLException e){
            System.out.println("Eroare in DataSource, LoginMapper, getId(String username, String password)" + e);
        }
        return log;
    }

    public static void insert(int id,String username, String password) {
        try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String statement = "INSERT INTO login(login_id,username, password,role) VALUES(?,?,?,?)";
            PreparedStatement prepSt = con.prepareStatement(statement);
            prepSt.setInt(1,id);
            prepSt.setString(2, username);
            prepSt.setString(3, password);
            prepSt.setInt(4,0);
            prepSt.executeUpdate();

        } catch (SQLException e) {
            System.out.println("Eroare in DataSource, LoginMapper, insert(String username, String password)" + e);
        }
    }
    
    public static void delete(Connection con, int id) throws SQLException {
		String statement = "DELETE FROM login where login_id=?";
		PreparedStatement prepSt = con.prepareStatement(statement);
		prepSt.setInt(1, id);
		prepSt.executeUpdate();
	}

	public static void update(Connection con, int id, String username, String password, String role)
			throws SQLException {
		String statement = "UPDATE login SET password=?, role=?, username=? where login_id=?";
		PreparedStatement prepSt = con.prepareStatement(statement);
		prepSt.setString(1, password);
		prepSt.setString(2, role);
		prepSt.setString(3, username);
		prepSt.setInt(4, id);		
		prepSt.executeUpdate();
}
}
