package DataSourceLogic;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.DefaultTableModel;

import DomainLogic.Account;
public class AccountMapper {
public void getAccountById(Connection connection, int accout_id) throws SQLException {
		
		String sqlQuery = "SELECT accout_id, amount, client_id, type FROM account where accout_id =?";
		PreparedStatement prepSt = connection.prepareStatement(sqlQuery);
		prepSt.setInt(1, accout_id);
		ResultSet queryResult = prepSt.executeQuery();

		// extract data from result set
		while (queryResult.next()) {
			// retrieve by column name
			int a_id = queryResult.getInt("accout_id");
			double amount = queryResult.getDouble("amount");
			int client_id = queryResult.getInt("client_id");
			String type = queryResult.getString("type");
			//populate the found student fields

			// display values
			System.out.println("Account id: " + a_id + ", Amount: " + amount + ", Client id: " + client_id + ", Type: " + type);
			
		}
	}
	
	public static String getType(int accout_id)
	{
		String type ="";
		try {
			DBConnection db = DBConnection.getConnection();
	        Connection con = db.connection;

	        String sql = "SELECT type FROM account WHERE accout_id = ?";
	        PreparedStatement prepSt = con.prepareStatement(sql);

	        prepSt.setInt(1, accout_id);
	        
	        ResultSet rs = prepSt.executeQuery();
	        
	        
	        while(rs.next())
	        {
	        	type = rs.getString("type");
	        		return type;
	        		
	        }
	        rs.close();
	        return type;
			}
			catch (Exception e) {
		        System.out.println("Eroare in Domain, AccountMapper, getType(int accout_id)" + e);
		        return type;
			}
	}
	
	public static double getAmount(int accout_id)
	{
		double amount =0;
		try {
			DBConnection db = DBConnection.getConnection();
	        Connection con = db.connection;

	        String sql = "SELECT amount FROM account WHERE accout_id = ?";
	        PreparedStatement prepSt = con.prepareStatement(sql);

	        prepSt.setInt(1, accout_id);
	        
	        ResultSet rs = prepSt.executeQuery();
	        
	        
	        while(rs.next())
	        {
	        	amount = rs.getDouble("amount");
	        		return amount;
	        		
	        }
	        rs.close();
	        return amount;
			}
			catch (Exception e) {
		        System.out.println("Eroare in Domain, AccountMapper, getAmount(int accout_id)" + e);
		        return amount;
			}
	}
	
	public static boolean verifyAmount(double amount, int accout_id)
	{
		try {
		DBConnection db = DBConnection.getConnection();
        Connection con = db.connection;

        String sql = "SELECT amount FROM account WHERE accout_id = ?";
        PreparedStatement prepSt = con.prepareStatement(sql);

        prepSt.setInt(1, accout_id);
        
        ResultSet rs = prepSt.executeQuery();
        double money=0;
        
        while(rs.next())
        {
        	money = rs.getDouble("amount");
        	if(money>=amount)
        		return true;
        		
        }
        rs.close();
        return false;
		}
		catch (Exception e) {
	        System.out.println("Eroare in Domain, AccountMapper, verifyAmount(double amount, int accout_id)" + e);
	        return false;
		}
		
	}
	
	public static void UpdateAmount( int AccId, double amount)
	{double d=0;
		try {
			DBConnection db = DBConnection.getConnection();
	        Connection con = db.connection;
	        
	        String sql = "SELECT amount FROM account WHERE accout_id = ?";
	        PreparedStatement prepSt = con.prepareStatement(sql);
	        prepSt.setInt(1, AccId);
	        double money=0;
	        ResultSet rs = prepSt.executeQuery();
	        while(rs.next())
	        {
	        money = rs.getDouble("amount");	
	           d= amount+money;		
	        }
	        rs.close();
	        
			String sql2 = "UPDATE account SET amount=? WHERE accout_id = ?";
	    	PreparedStatement prepSt2 = con.prepareStatement(sql2);  	
	        prepSt2.setDouble(1, d);
	        prepSt2.setInt(2, AccId);
	        prepSt2.executeUpdate();
			
		} catch (Exception e) {
	        System.out.println("Eroare in Domain, AccountMapper, updateAmount(int client_id, int AccId, double initialSum, double amount)" + e);
		}
	}
	
	public static DefaultTableModel viewAccountsTable(DefaultTableModel model)
	{
		try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sql = "SELECT * FROM account";
            PreparedStatement prepSt = con.prepareStatement(sql);

            ResultSet rs = prepSt.executeQuery();
            while(rs.next()){
            	
            	int accout_id = rs.getInt("accout_id");
    			int client_id = rs.getInt("client_id");
    			Double amount = rs.getDouble("amount");
    			String type = rs.getString("type");
    			model.addRow(new Object[]{accout_id,client_id,amount,type});
            }
            rs.close();
            return model;
        } catch (Exception f) {
            System.out.println("Eroare in AccountMapper, viewAccountsTable(DefaultTableModel model)" + f);}
		return model;
	}
	
	public static DefaultTableModel ViewAccountsTable(DefaultTableModel model, int client_id)
	{
		try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sql = "SELECT * FROM account WHERE client_id = ?";
            PreparedStatement prepSt = con.prepareStatement(sql);

            prepSt.setInt(1, client_id);

            ResultSet rs = prepSt.executeQuery();
            while(rs.next()){
    			int accout_id = rs.getInt("accout_id");
    			double amount = rs.getDouble("amount");
    			String type = rs.getString("type");
    			model.addRow(new Object[]{accout_id,amount,type});
            }
            rs.close();
            return model;
        } catch (Exception f) {
            System.out.println("Eroare in AccountMapper, ViewAccountsTable(DefaultTableModel model, int client_id)" + f);}
		return model;
	}

	public static void printAccount(Connection connection) throws SQLException {
		// execute a query

		System.out.println("Creating Statement...");
		Statement stmt = connection.createStatement();
		String sqlQuery = "SELECT * FROM account";
		ResultSet queryResult = stmt.executeQuery(sqlQuery);

		// extract data from result set
		while (queryResult.next()) {
			// retrieve by column name
			int a_id = queryResult.getInt("accout_id");
			double amount = queryResult.getDouble("amount");
			int client_id = queryResult.getInt("client_id");
			String type = queryResult.getString("type");
			// display values
			System.out.println("Account id: " + a_id + ", Amount: " + amount + ", Client id: " + client_id + ", Type: " + type);
}
		queryResult.close();
		stmt.close();
	}
	
	public static void insertAccount(Account acc) throws SQLException {
		try {
			DBConnection db = DBConnection.getConnection();
	        Connection con = db.connection;
		String statement = "INSERT INTO account (amount, client_id, type) VALUES (?, ?, ?)";
		PreparedStatement prepSt = con.prepareStatement(statement);
		// prepSt.setInt(1, student.getStudentID());
		prepSt.setDouble(1, acc.getAmount());
		prepSt.setInt(2, acc.getClient_id());
		prepSt.setString(3, acc.getType());

		prepSt.executeUpdate();
		}
		catch(Exception e) {
	        System.out.println("Eroare in Domain, AccountMapper, insertAccount(Account acc)" + e);
		}
	}
	
	public static void getAccounts(int client_id)
	{
		 try {
	        	DBConnection db = DBConnection.getConnection();
	            Connection con = db.connection;

	            String sql = "SELECT * FROM account WHERE client_id = ?";
	            PreparedStatement prepSt = con.prepareStatement(sql);

	            prepSt.setInt(1, client_id);

	            ResultSet rs = prepSt.executeQuery();
	            while(rs.next()){
	            	String address = rs.getString("address");
	    			int c_id = rs.getInt("client_id");
	    			String cnp = rs.getString("cnp");
	    			int id_card = rs.getInt("id_card");
	    			int log_id = rs.getInt("login_id");
	    			String name = rs.getString("name");
	            }
	            rs.close();
	            
	        } catch (Exception e) {
	            System.out.println("Eroare in Domain, AccountMapper, getAccounts(int client_id)" + e);
	        }
	}

	public static boolean verifyAccountId( int accout_id)
	{
		try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sql = "SELECT * FROM account WHERE accout_id = ?";
            PreparedStatement prepSt = con.prepareStatement(sql);

            prepSt.setInt(1, accout_id);
            
            ResultSet rs = prepSt.executeQuery();
            while(rs.next())
            {
            	
            	return true;
            }
            rs.close();
            return false;
            
        } catch (Exception e) {
            System.out.println("Eroare in Domain, AccountMapper, verifyAccountId(int accout_id)" + e);
            return false;
        }
	}
	
	public static boolean verifyAccountOwner(int client_id, int accout_id)
	{
		try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sql = "SELECT * FROM account WHERE accout_id = ? AND client_id=?";
            PreparedStatement prepSt = con.prepareStatement(sql);

            prepSt.setInt(1, accout_id);
            prepSt.setInt(2, client_id);
            
            ResultSet rs = prepSt.executeQuery();
            while(rs.next())
            {
            	
            	return true;
            }
            rs.close();
            return false;
            
        } catch (Exception e) {
            System.out.println("Eroare in Domain, AccountMapper, verifyAccountOwner(int client_id, int accout_id)" + e);
            return false;
        }
	}
	
	public static boolean verifyAccounts(int client_id, int dst_client_id, int SrcAccId, int DstAccId)
	{
		try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sql = "SELECT * FROM account WHERE client_id = ?";
            PreparedStatement prepSt = con.prepareStatement(sql);

            prepSt.setInt(1, client_id);
            
            boolean SrcAcc = false;
            boolean DstAcc = false;


            ResultSet rs = prepSt.executeQuery();
            while(rs.next())
            {
            	int a_id = rs.getInt("accout_id");
            	
            	if(a_id == SrcAccId)
            		{SrcAcc = true;
            		break;}
            	
            }
            
            sql = "SELECT * FROM account WHERE client_id = ?";
            prepSt = con.prepareStatement(sql);

            prepSt.setInt(1, dst_client_id);
           
            rs = prepSt.executeQuery();
            while(rs.next())
            {
            	
            	int a_id = rs.getInt("accout_id");
          
            	if(a_id==DstAccId)
            		{DstAcc=true;
            		break;}
            }
            
            rs.close();
            if(SrcAcc && DstAcc)
            	return true;
            return false;
            
        } catch (Exception e) {
            System.out.println("Eroare in Domain, AccountMapper, verifyAccounts(int client_id, int SrcAccId, int DstAccId)" + e);
            return false;
        }
	}
	
	public static void delete(int id) {
		try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

		String statement = "DELETE FROM account where accout_id=?";
		PreparedStatement prepSt = con.prepareStatement(statement);
		prepSt.setInt(1, id);
		prepSt.executeUpdate();}
		catch (Exception e) {
            System.out.println("Eroare in Domain, AccountMapper, delete(int id)" + e);}
		
	}

	public static void update(Account acc)
			 {
		try {
		DBConnection db = DBConnection.getConnection();
        Connection con = db.connection;
		String statement = "UPDATE account SET amount=?, client_id=?, type=? where accout_id=?";
		PreparedStatement prepSt = con.prepareStatement(statement);
		
		prepSt.setDouble(1, acc.getAmount());
		prepSt.setInt(2, acc.getClient_id());
		prepSt.setString(3, acc.getType());
		prepSt.setInt(4, acc.getAcc_id());
		prepSt.executeUpdate();}
		catch (Exception e) {
            System.out.println("Eroare in Domain, AccountMapper, update(Account acc)" + e);}
		
}
}
