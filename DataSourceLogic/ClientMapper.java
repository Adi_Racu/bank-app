package DataSourceLogic;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.DefaultTableModel;

import DomainLogic.Client;

public class ClientMapper {

	
	public static int getClientId(int login_id)
	{
		try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sqlQuery = "SELECT Client_id FROM client where log_id =?";
    		PreparedStatement prepSt = con.prepareStatement(sqlQuery);
    		prepSt.setInt(1, login_id);
    		ResultSet queryResult = prepSt.executeQuery();
    		int c_id=0;
    		// extract data from result set
    		while (queryResult.next()) {
    			c_id = queryResult.getInt("client_id");
    			    			
    		}
    		queryResult.close();
          return c_id;
            
        } catch (Exception e) {
            System.out.println("Eroare in Domain, ClientLogic, getClientId(int login_id)" + e);
            return -1;
        }
	}

	public void getCLientByClient_id(Connection connection, int client_id) throws SQLException {
		
		String sqlQuery = "SELECT address, client_id, cnp, id_card, login_id, name FROM client where client_id =?";
		PreparedStatement prepSt = connection.prepareStatement(sqlQuery);
		prepSt.setInt(1, client_id);
		ResultSet queryResult = prepSt.executeQuery();

		// extract data from result set
		while (queryResult.next()) {
			// retrieve by column name
			String address = queryResult.getString("address");
			int c_id = queryResult.getInt("client_id");
			String cnp = queryResult.getString("cnp");
			int id_card = queryResult.getInt("id_card");
			int log_id = queryResult.getInt("login_id");
			String name = queryResult.getString("name");
			//populate the found student fields

			// display values
			System.out.println("Client id: " + c_id + ", Name: " + name + ", Address: " + address + ", CNP: " + cnp + ", Id card: " + id_card + ", Login id: " + log_id);
			
		}
	}
	
	
	public static DefaultTableModel viewClientsTable(DefaultTableModel model)
	{
		try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sql = "SELECT * FROM client";
            PreparedStatement prepSt = con.prepareStatement(sql);

            ResultSet rs = prepSt.executeQuery();
            while(rs.next()){
            	int client_id = rs.getInt("Client_id");
    			String name = rs.getString("Name");
    			int login_id = rs.getInt("log_id");
    			int card_id = rs.getInt("Id_card");
    			model.addRow(new Object[]{client_id,name,login_id,card_id});
            }
            rs.close();
            return model;
        } catch (Exception f) {
            System.out.println("Eroare in ClientMapper, viewClientsTable(DefaultTableModel model)" + f);}
		return model;
	}
	
	public static void printClients(Connection connection) throws SQLException {
		// execute a query

		System.out.println("Creating Statement...");
		Statement stmt = connection.createStatement();
		String sqlQuery = "SELECT * FROM client";
		ResultSet queryResult = stmt.executeQuery(sqlQuery);

		// extract data from result set
		while (queryResult.next()) {
			// retrieve by column name
			String address = queryResult.getString("address");
			int c_id = queryResult.getInt("client_id");
			String cnp = queryResult.getString("cnp");
			int id_card = queryResult.getInt("id_card");
			int log_id = queryResult.getInt("login_id");
			String name = queryResult.getString("name");
			// display values
			System.out.println("Client id: " + c_id + ", Name: " + name + ", Address: " + address + ", CNP: " + cnp + ", Id card: " + id_card + ", Login id: " + log_id);
		}
		queryResult.close();
		stmt.close();
	}
	
	public static void insertClient(Client client) throws SQLException {
		try {
			DBConnection db = DBConnection.getConnection();
	        Connection con = db.connection;
		String statement = "INSERT INTO client (Name, ID_card,log_id) VALUES (?, ?, ?)";
		PreparedStatement prepSt = con.prepareStatement(statement);
		// prepSt.setInt(1, student.getStudentID());
		prepSt.setString(1, client.getName());
		prepSt.setInt(2, client.getId_card());
		prepSt.setInt(3, client.getLogin_id());
	

		prepSt.executeUpdate();}
		catch(Exception e) {
	        System.out.println("Eroare in Domain, ClientLogic, newClient(String name, String address, String cnp, int login_id, int card_id)" + e);
		}
	}

	public static void delete(int id) throws SQLException {
		try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;
            
		String statement = "DELETE FROM client where client_id=?";
		PreparedStatement prepSt = con.prepareStatement(statement);
		prepSt.setInt(1, id);
		prepSt.executeUpdate();
		
		}catch (Exception e) {
			System.out.println("Eroare in Domain, ClientLogic, dropClient(int client_id)" + e);}
		
	}

	
	public static boolean verifyClientId(int client_id)
	{
		try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sql = "SELECT * FROM client WHERE client_id = ?";
            PreparedStatement prepSt = con.prepareStatement(sql);

            prepSt.setInt(1, client_id);
            
            ResultSet rs = prepSt.executeQuery();
            while(rs.next())
            {
            	
            	return true;
            }
            rs.close();
            return false;
            
        } catch (Exception e) {
            System.out.println("Eroare in Domain, ClientMapper, verifyClientId(int client_id)" + e);
            return false;
        }
	}
	
	public static void update(Client c)
	{
	try {
		DBConnection db = DBConnection.getConnection();
        Connection con = db.connection;
        
		String statement = "UPDATE client SET name=?, id_card=?, login_id=?, where client_id=?";
		PreparedStatement prepSt = con.prepareStatement(statement);
		prepSt.setString(1, c.getName());
		prepSt.setInt(2, c.getId_card());
		prepSt.setInt(4, c.getLogin_id());
		prepSt.setInt(6, c.getClient_id());
		prepSt.executeUpdate();
}
	catch (Exception e) {
        System.out.println("Eroare in Domain, ClientMapper, update(Client c)" + e);}
	
}
}
