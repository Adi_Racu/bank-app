package DomainLogic;

import java.util.Scanner;

public class Client {
	private String name;
	private int id_card;
	private int login_id;
	private int client_id;
	

	public Client(String name, int id_card, int login_id)
	{
		setName(name);
		setId_card(id_card);
		setLogin_id(login_id);
	}
	
	public Client()
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Name: ");
		String s = sc.nextLine();
		setName(s);
		System.out.println("Card id: ");
		int n = Integer.parseInt(sc.nextLine());
		setId_card(n);
		System.out.println("Login id: ");
		n = Integer.parseInt(sc.nextLine());
		setLogin_id(n);
		
	}

	
	//Setters
	public void setName(String name)
	{
		this.name=name;
	}
	
	public void setId_card(int id_card)
	{
		this.id_card=id_card;
	}
	
	
	public void setLogin_id(int login_id)
	{
		this.login_id=login_id;
	}
	
	public void setClient_id(int client_id)
	{
		this.client_id=client_id;
	}
	
	//Getters
	public String getName()
	{
		return this.name;
	}
	
	public int getId_card()
	{
		return this.id_card;
	}
	

	public int getLogin_id()
	{
		return this.login_id;
	}
	
	public int getClient_id()
	{
		return this.client_id;
	}
}
