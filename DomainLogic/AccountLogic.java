package DomainLogic;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.table.DefaultTableModel;

import DataSourceLogic.AccountMapper;
import DataSourceLogic.DBConnection;

public class AccountLogic {
	public static DefaultTableModel AccountTable(DefaultTableModel model)
	{
		model = AccountMapper.viewAccountsTable(model);
		return model;
	}
	
	public static void newAccount(int client_id, double amount, String type) throws SQLException
	{
		Account a = new Account(client_id,amount,type);
		AccountMapper.insertAccount(a);
	}
	
	public static void transfer(int id,long amount) {
		AccountMapper.UpdateAmount(id, amount);
	}
	
	public static DefaultTableModel AccountTable(DefaultTableModel model, int client_id)
	{
		model = AccountMapper.ViewAccountsTable(model, client_id);
		return model;
	}
	
	public static void getAccounts(int client_id)
	{
       AccountMapper.getAccounts(client_id);
	}
	
	public static boolean verifyAccounts(int client_id, int dst_client_id, int SrcAccId, int DstAccId)
	{
		return AccountMapper.verifyAccounts(client_id, dst_client_id, SrcAccId, DstAccId);
	}
	
	
	public static boolean verifyAccountOwner(int client_id, int acc_id)
	{
		return AccountMapper.verifyAccountOwner(client_id, acc_id);
	}
	
	public static boolean verifyAccountId( int acc_id)
	{
		return AccountMapper.verifyAccountId(acc_id);
	}
	
	public static void updateAccount(int acc_id, int client_id, double amount, String type)
	{
		Account a = new Account(client_id, amount, type);
		a.setAcc_id(acc_id);
		AccountMapper.update(a);
	}
	
	public static void dropAccount(int acc_id)
	{
           AccountMapper.delete(acc_id);
		
	}
}
