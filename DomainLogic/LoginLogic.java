package DomainLogic;

import DataSourceLogic.LoginMapper;

public class LoginLogic {
	public static boolean verifyLoginAdmin(String username, String password)
	{
		return LoginMapper.getLoginAdmin(username, password);
	}
	
	public static boolean verifyLoginClient(String username, String password)
	{
		return LoginMapper.getLoginClient(username, password);
	}
	
	public static Login getLoginByUsername(String username)
	{
		Login l = LoginMapper.getLoginByUsername(username);
		return l;
	}
	
	public static boolean verifyLoginId(int login_id)
	{
		return LoginMapper.verifyLoginId(login_id);
	}
	
	public static int getLoginID(String username,String password)
	{
	int id=LoginMapper.getLoginId(username,password);
	
		return id;
	}
	public static void insert_Login(int id, String user,String pass) {
		LoginMapper.insert(id, user, pass);
	}
	
	public static int getRole(boolean admin, boolean client)
	{
		if(admin)
			return 1;
		if(client)
			return 0;
		return -1;
	}
	
	public static Login getId(String username, String password)
	{
		return LoginMapper.getId(username, password);
	}
	
	
}
