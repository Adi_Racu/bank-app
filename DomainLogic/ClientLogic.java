package DomainLogic;

import java.sql.SQLException;

import javax.swing.table.DefaultTableModel;

import DataSourceLogic.AccountMapper;
import DataSourceLogic.ClientMapper;

public class ClientLogic {

	public static int getClientId(int login_id)
	{
        return ClientMapper.getClientId(login_id);
	}
	
	public static void dropClient(int client_id) throws SQLException
	{
            ClientMapper.delete(client_id);
	}
	
	
	
	public static DefaultTableModel ClientTable(DefaultTableModel model)
	{
		model = ClientMapper.viewClientsTable(model);
		return model;
	}
	
	
	
	
	public static void newClient(String name, int login_id, int card_id) throws SQLException
	{
		
		Client c = new Client(name, card_id, login_id);
		ClientMapper.insertClient(c);
		
	}
	
	
	
	public static void updateClient(int client_id, String name, int login_id, int card_id)
	{
		Client c = new Client(name, card_id, login_id);
		c.setClient_id(client_id);
		ClientMapper.update(c);
		
	}
	
	public static boolean verifyClientId(int client_id)
	{
		return ClientMapper.verifyClientId(client_id);
	}
}
