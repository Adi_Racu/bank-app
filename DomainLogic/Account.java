package DomainLogic;
import java.util.Scanner;
public class Account {
	int acc_id;
	int client_id;
	String type;
	double amount;
	
	public Account(int client_id, double amount, String type)
	{
		
		setClient_id(client_id);
		setType(type);
		setAmount(amount);
		}
	
	public Account()
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Account id: ");
		int n = sc.nextInt();
		setAcc_id(n);
		System.out.println("Client id: ");
		n = sc.nextInt();
		setClient_id(n);
		System.out.println("Type: ");
		String s= sc.nextLine();
		setType(s);
		System.out.println("Amount: ");
		n = sc.nextInt();
		setAmount(n);	
	}

	//Setters
	public void setAcc_id(int acc_id)
	{
		this.acc_id=acc_id;
	}
	
	public void setClient_id(int client_id)
	{
		this.client_id=client_id;
	}
	
	public void setType(String type)
	{
		this.type=type;
	}
	
	public void setAmount(double amount)
	{
		this.amount=amount;
	}
	
	//Getters
	public int getAcc_id()
	{
		return this.acc_id;
	}
	
	public int getClient_id()
	{
		return this.client_id;
	}
	
	public String getType()
	{
		return this.type;
	}

	public double getAmount()
	{
		return this.amount;
	}
}
