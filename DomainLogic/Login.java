package DomainLogic;

public class Login {

	int login_id;
	String username;
	String password;
	int role;
	
	public Login(int login_id, String username, String password, int role)
	{
		setLogin_id(login_id);
		setUsername(username);
		setPassword(password);
		setRole(role);
		}
	
	public Login( String username, String password)
	{
		setUsername(username);
		setPassword(password);
		}
	
	
	public Login()
	{
		
		setUsername("");
		setPassword("");
		setRole(-1);	
	}

	//Setters
	public void setLogin_id(int login_id)
	{
		this.login_id=login_id;
	}
	
	public void setUsername(String username)
	{
		this.username=username;
	}
	
	public void setPassword(String password)
	{
		this.password=password;
	}
	
	public void setRole(int role)
	{
		this.role=role;
	}
	
	//Getters
	public int getLogin_id()
	{
		return this.login_id;
	}
	
	public String getUsername()
	{
		return this.username;
	}
	
	public String getPassword()
	{
		return this.password;
	}

	public int getRole()
	{
		return this.role;
	}
}
