package Presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import DataSourceLogic.AccountMapper;
import DomainLogic.AccountLogic;
import DomainLogic.ClientLogic;

import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class Client extends JFrame {

	private JPanel contentPane;
	private JTable table;
	String columnNames[]= {"Account ID", "amount", "type"};
	Object[][] rowdata = {};
	DefaultTableModel model = new DefaultTableModel(rowdata,columnNames);
	private JTextField AC_ID_SEND;
	private JTextField AC_ID_RECive;
	private JTextField sum;
	
	public Client(int id) {
		init(id);
		//clear_table();
		//refreshTable(id);
	}
	 public void refreshTable(int id) {
		 DefaultTableModel model = (DefaultTableModel) table.getModel();
		 AccountLogic.AccountTable(model, id);
		  }
		  public void clear_table() {
			  model.setRowCount(0); 
		  }	 

	
	public void init(int id) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 440, 349);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton see = new JButton("View");
		see.setBounds(40, 26, 89, 23);
		contentPane.add(see);
		
		see.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clear_table();
			   refreshTable(id);
			}
		});
		
		
		JButton Trans = new JButton("Transfer");
		Trans.setBounds(40, 176, 89, 23);
		contentPane.add(Trans);
		Trans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int sed_id=Integer.parseInt(AC_ID_SEND.getText());
				int rec_id = Integer.parseInt(AC_ID_RECive.getText());
				int sum_sent = Integer.parseInt(sum.getText());
				if(AccountMapper.verifyAmount(sum_sent, sed_id)) {
					AccountLogic.transfer(sed_id,(-sum_sent));
					AccountLogic.transfer(rec_id,sum_sent);
				}
				clear_table();
			   refreshTable(id);
			}
		});
		
		
		
		JScrollPane panel = new JScrollPane();
		panel.setBackground(Color.PINK);
		panel.setBounds(183, 23, 217, 140);
		contentPane.add(panel);
        table=new JTable(model);
		
		panel.setViewportView(table);
		
		AC_ID_SEND = new JTextField();
		AC_ID_SEND.setBounds(40, 227, 75, 20);
		contentPane.add(AC_ID_SEND);
		AC_ID_SEND.setColumns(10);
		
		AC_ID_RECive = new JTextField();
		AC_ID_RECive.setBounds(40, 280, 75, 20);
		contentPane.add(AC_ID_RECive);
		AC_ID_RECive.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("From");
		lblNewLabel.setBounds(40, 210, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("To");
		lblNewLabel_1.setBounds(40, 266, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		sum = new JTextField();
		sum.setBounds(169, 227, 75, 20);
		contentPane.add(sum);
		sum.setColumns(10);
		
		JLabel lblSum = new JLabel("Sum");
		lblSum.setBounds(169, 210, 46, 14);
		contentPane.add(lblSum);
	}
}
