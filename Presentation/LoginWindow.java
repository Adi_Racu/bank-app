package Presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import DomainLogic.ClientLogic;
import DomainLogic.LoginLogic;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginWindow extends JFrame {

	private JPanel contentPane;
	private JTextField name;
	private JTextField password;
    LoginLogic log=new LoginLogic();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginWindow frame = new LoginWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 275, 242);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		name = new JTextField();
		name.setBounds(160, 57, 86, 20);
		contentPane.add(name);
		name.setColumns(10);
		
		password = new JTextField();
		password.setBounds(160, 141, 86, 20);
		contentPane.add(password);
		password.setColumns(10);
		
		JButton admin = new JButton("Admin");
		admin.setBounds(21, 57, 89, 23);
		contentPane.add(admin);
		
		admin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = name.getText();
				String pass =  password.getText();
			if(LoginLogic.verifyLoginAdmin(username, pass)==true) {
				Admin wind2=new Admin();
				wind2.setVisible(true);
			}
			else {
				 System.out.println("eroare"); 
			}
			}});
		
		JButton user = new JButton("User");
		user.setBounds(21, 141, 89, 23);
		contentPane.add(user);
		   
		user.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = name.getText();
				String pass =  password.getText();
			if(LoginLogic.verifyLoginClient(username, pass)) {
			int log_id=LoginLogic.getLoginID(username, pass);
			int id_client=ClientLogic.getClientId(log_id);
			System.out.println(id_client); 
				Client wind1=new Client(id_client);
				wind1.setVisible(true);
				
			}
			}});
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel.setBounds(160, 22, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPassword.setBounds(160, 109, 86, 14);
		contentPane.add(lblPassword);
	}
}
