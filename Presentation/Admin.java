package Presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import DomainLogic.AccountLogic;
import DomainLogic.ClientLogic;
import DomainLogic.LoginLogic;

import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTable;

public class Admin extends JFrame {

	private JPanel contentPane;
	private JTextField nume_field;
	private JTextField id_field;
	private JTextField log_field;
	private JTextField cleint_field;
	private JTable table;
	private JTable table2;
	private JTextField type_field;
	private JTextField amount;
	private JTextField client_acc_id;
	String columnNames[]= {"Client ID", "Name", "log_id", "Card_id"};
    String columnNames2[]= {"Account ID", "client_id", "amount"};
	Object[][] rowdata = {};
	Object[][] rowdata2 = {};
	DefaultTableModel model = new DefaultTableModel(rowdata,columnNames);
	DefaultTableModel model2 = new DefaultTableModel(rowdata2,columnNames2);
	private JTextField Account_ID;
	private JTextField username;
	private JTextField txtPassword;
	
	public Admin() {
		init();
		clear_table();
		clear_table2();
		refreshTable();
		refreshTable2();
	}
	

	
	 public void refreshTable() {
	 DefaultTableModel model = (DefaultTableModel) table.getModel();
	 ClientLogic.ClientTable(model);
	  }
	  public void clear_table() {
		  model.setRowCount(0); 
	  }	 

	  public void refreshTable2() {
			 DefaultTableModel model2 = (DefaultTableModel) table2.getModel();
			 AccountLogic.AccountTable(model2);
			  }
			  public void clear_table2() {
				  model2.setRowCount(0); 
			  }	 
	  
	  
	  
	public void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 682, 572);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		nume_field = new JTextField();
		nume_field.setBounds(179, 30, 86, 20);
		contentPane.add(nume_field);
		nume_field.setColumns(10);
		
		id_field = new JTextField();
		id_field.setBounds(179, 85, 86, 20);
		contentPane.add(id_field);
		id_field.setColumns(10);
		
		log_field = new JTextField();
		log_field.setBounds(179, 142, 86, 20);
		contentPane.add(log_field);
		log_field.setColumns(10);
		
		cleint_field = new JTextField();
		cleint_field.setColumns(10);
		cleint_field.setBounds(179, 201, 86, 20);
		contentPane.add(cleint_field);
		
		JLabel lblNewLabel = new JLabel("Nume");
		lblNewLabel.setBounds(179, 11, 72, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblIdcard = new JLabel("ID_card");
		lblIdcard.setBounds(179, 71, 86, 14);
		contentPane.add(lblIdcard);
		
		JLabel lblLogid = new JLabel("log_id");
		lblLogid.setBounds(179, 129, 86, 14);
		contentPane.add(lblLogid);
		
		JLabel lblClientid = new JLabel("Client_ID");
		lblClientid.setBounds(179, 187, 86, 14);
		contentPane.add(lblClientid);
		
		JButton Add_client = new JButton("Add_client");
		Add_client.setBounds(10, 29, 89, 23);
		contentPane.add(Add_client);
		
		Add_client.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = nume_field.getText();
				int id_card = Integer.parseInt(id_field.getText());
				int login_id = Integer.parseInt(log_field.getText());
				String user = username.getText();
				String pass = txtPassword.getText();
				try {
					ClientLogic.newClient(name, login_id, id_card);
					LoginLogic.insert_Login(login_id, user, pass);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				clear_table();
				refreshTable();
			}
		});
		
		JButton delete = new JButton("Delete_client");
		delete.setBounds(10, 84, 95, 23);
		contentPane.add(delete);
		
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int client_id = Integer.parseInt(cleint_field.getText());
				try {
					ClientLogic.dropClient(client_id);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				clear_table();
				refreshTable();
			}
		});
		
		
		JButton update = new JButton("Update");
		update.setBounds(10, 141, 89, 23);
		contentPane.add(update);
		
		update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = nume_field.getText();
				int id_card = Integer.parseInt(id_field.getText());
				int login_id = Integer.parseInt(log_field.getText());
				int client_id = Integer.parseInt(cleint_field.getText());
				
			   ClientLogic.updateClient(client_id, name, login_id, id_card);
				
				clear_table();
				refreshTable();
			}
		});
		
		JScrollPane panel = new JScrollPane();
		panel.setBackground(Color.PINK);
		panel.setBounds(347, 30, 300, 191);
		contentPane.add(panel);
		
	
		table=new JTable(model);
		
		panel.setViewportView(table);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBackground(Color.PINK);
		scrollPane_1.setBounds(347, 314, 300, 196);
		contentPane.add(scrollPane_1);	
		
		table2 = new JTable(model2);
		scrollPane_1.add(table2);
		scrollPane_1.setViewportView(table2);
		
		type_field = new JTextField();
		type_field.setColumns(10);
		type_field.setBounds(179, 336, 86, 20);
		contentPane.add(type_field);
		
		JLabel Type = new JLabel("Type");
		Type.setBounds(179, 317, 86, 14);
		contentPane.add(Type);
		
		JLabel lblAmout = new JLabel("Amount");
		lblAmout.setBounds(179, 377, 86, 14);
		contentPane.add(lblAmout);
		
		amount = new JTextField();
		amount.setColumns(10);
		amount.setBounds(179, 391, 86, 20);
		contentPane.add(amount);
		
		JLabel client_id = new JLabel("Client_Id");
		client_id.setBounds(179, 435, 86, 14);
		contentPane.add(client_id);
		
		client_acc_id = new JTextField();
		client_acc_id.setColumns(10);
		client_acc_id.setBounds(179, 448, 86, 20);
		contentPane.add(client_acc_id);
		
		JButton btnAddaccount = new JButton("Add_account");
		btnAddaccount.setBounds(10, 333, 95, 23);
		contentPane.add(btnAddaccount);
		
		btnAddaccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String type = type_field.getText();
				int ammount = Integer.parseInt(amount.getText());
				int client_id = Integer.parseInt(client_acc_id.getText());
				
				try {
					AccountLogic.newAccount(client_id, ammount, type);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				clear_table2();
				refreshTable2();
			}
		});
		
		Account_ID = new JTextField();
		Account_ID.setColumns(10);
		Account_ID.setBounds(179, 503, 86, 20);
		contentPane.add(Account_ID);
		
		JButton btnDeleteaccount = new JButton("Delete_account");
		btnDeleteaccount.setBounds(10, 388, 107, 23);
		contentPane.add(btnDeleteaccount);
		
		btnDeleteaccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String type = type_field.getText();
				int acc_id=Integer.parseInt(Account_ID.getText());
				int ammount = Integer.parseInt(amount.getText());
				int client_id = Integer.parseInt(client_acc_id.getText());
				
				AccountLogic.dropAccount(acc_id);
				
				clear_table2();
				refreshTable2();
			}
		});
		
		JButton Update_account = new JButton("Update");
		Update_account.setBounds(10, 445, 89, 23);
		contentPane.add(Update_account);
		
		Update_account.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String type = type_field.getText();
				int acc_id=Integer.parseInt(Account_ID.getText());
				int ammount = Integer.parseInt(amount.getText());
				int client_id = Integer.parseInt(client_acc_id.getText());
				AccountLogic.updateAccount(acc_id, client_id, ammount, type);
				clear_table2();
				refreshTable2();
			}
		});
		
		
		JLabel lblAccountid = new JLabel("Account_ID");
		lblAccountid.setBounds(179, 490, 72, 14);
		contentPane.add(lblAccountid);
		
		username = new JTextField();
		username.setBounds(179, 248, 86, 20);
		contentPane.add(username);
		username.setColumns(10);
		
		JLabel Usename = new JLabel("Username");
		Usename.setBounds(179, 232, 72, 14);
		contentPane.add(Usename);
		
		txtPassword = new JTextField();
		txtPassword.setColumns(10);
		txtPassword.setBounds(289, 248, 86, 20);
		contentPane.add(txtPassword);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(289, 232, 72, 14);
		contentPane.add(lblPassword);
		
		
		
		
		
	}
}
